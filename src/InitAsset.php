<?php
namespace fafcms\assets\init;

use yii\web\AssetBundle;

class InitAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/assets';

    public $js = [
        'js/init.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
