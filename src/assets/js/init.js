;(function (factory) {
    'use strict'
    window.fafcms = window.fafcms || factory(window.jQuery)
})(function ($) {
    'use strict'
    function fafcms(name, options) {
        this._name = name
        this.init()
    }

    $.extend(fafcms.prototype, {
        events: {
            'scroll': [],
            'ready': [],
            'windowLoad': [],
            'windowDragenter': [],
            'windowResize': [],
            'init': [],
            'initPlugins': []
        },
        runEvents: function(events, eventType, e) {
            if (typeof events[eventType] !== 'undefined') {
                events[eventType].forEach(function(eventAction) {
                    if (typeof eventAction === 'function') {
                        eventAction(e)
                    } else {
                        console.warn('Event of type ' + eventType + ' is not a function.', eventAction)
                    }
                })
            }
        },
        init: function() {
            var fafcmsApp = this;

            fafcmsApp.runEvents(fafcmsApp.events, 'init')

            if (this.events['scroll'].length > 0) {
                $(window).scroll(function (e) {
                    fafcmsApp.runEvents(fafcmsApp.events, 'scroll', e)
                })
            }

            $(document).ready(function (e) {
                if (fafcmsApp.events['ready'].length > 0) {
                    fafcmsApp.runEvents(fafcmsApp.events, 'ready', e)
                }

                if (fafcmsApp.events['initPlugins'].length > 0) {
                    fafcmsApp.runEvents(fafcmsApp.events, 'initPlugins', $('html'))
                }

                $(document).on('pjax:success', function(e) {
                   fafcmsApp.runEvents(fafcmsApp.events, 'initPlugins', $(e.target))
                })
            })

            if (this.events['windowDragenter'].length > 0) {
                $(window).on('dragenter', function (e) {
                    fafcmsApp.runEvents(fafcmsApp.events, 'windowDragenter', e)
                })
            }

            if (this.events['windowResize'].length > 0) {
                $(window).on('resize', function (e) {
                    fafcmsApp.runEvents(fafcmsApp.events, 'windowResize', e)
                })
            }

            if (this.events['windowLoad'].length > 0) {
                $(window).on('load', function (e) {
                    fafcmsApp.runEvents(fafcmsApp.events, 'windowLoad', e)
                })
            }
        }
    })

    fafcms.prototype.events['ready'].push(function () {
        fafcms.prototype.loader = '<div class="fafcms-loader center-align">' +
        '  <div class="preloader-wrapper big active">' +
        '    <div class="spinner-layer">' +
        '      <div class="circle-clipper left">' +
        '        <div class="circle"></div>' +
        '      </div>' +
        '      <div class="gap-patch">' +
        '        <div class="circle"></div>' +
        '      </div>' +
        '      <div class="circle-clipper right">' +
        '        <div class="circle"></div>' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '  <div>' + yii.t('app', 'Loading') + '</div>' +
        '</div>'
    })

    return fafcms
})
